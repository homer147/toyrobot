module.exports = function() {
	var src = './src/client/';
	var public = './public/';
	var js = public + 'js/';
	var server = './src/server';

	var config = {
		public: public,
		server: server,
		// HTML
		html: src + '**/*.html',
		images: src + 'images/**/*.*',
		imagesProd: public + 'images/',
		// CSS
		less: src + 'styles/styles.less',
		css: public + 'css/',
		// JS
		js: js,
		bowerLib: './bower_components/angular/angular.min.js',
		srcJs: src + 'app/app.js',
		prodLib: js + 'lib/',
		prodJs: js + 'app/',
		// NODE
		nodeServer: './src/server/app.js'
	};

	return config;
};
