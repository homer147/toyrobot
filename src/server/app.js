/*jshint node:true*/
'use strict';

var express = require('express');
var app = express();

var port = 51000;

app.use(express.static('./public/'));
app.use('/*', express.static('./public/index.html'));

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});
