(function() {
	'use strict';
	var app = angular.module('toyRobot', []);

	app.controller('gameBoardCtrl', ['$scope', function($scope) {
		var xAxisMax = 4;
		var yAxisMax = 4;
		$scope.showPosition = false;
		$scope.setPlace = false;
		$scope.values = [
	        {id: 'NORTH', 		label: 'NORTH'},
	        {id: 'SOUTH', 		label: 'SOUTH'},
	        {id: 'EAST', 		label: 'EAST'},
	        {id: 'WEST', 		label: 'WEST'}
		];
		$scope.unblock = false;
		$scope.valuesEntered = function() {
			if ($scope.xAxis && $scope.yAxis && $scope.facing.id) {
				$scope.unblock = true;
			}
			$scope.setPlace = false;
		};

		$scope.move = function() {
			switch($scope.facing.id) {
				case 'NORTH':
					if ($scope.yAxis < yAxisMax)
						$scope.yAxis += 1;
					break;
				case 'SOUTH':
					if ($scope.yAxis > 0)
						$scope.yAxis -= 1;
					break;
				case 'EAST':
					if ($scope.xAxis < xAxisMax)
						$scope.xAxis += 1;
					break;
				case 'WEST':
					if ($scope.xAxis > 0)
						$scope.xAxis -= 1;
					break;
				default:
					alert('direction unknown');
			}
		};

		$scope.left = function() {
			switch($scope.facing.id) {
				case 'NORTH':
					$scope.facing.id = 'WEST';
					break;
				case 'SOUTH':
					$scope.facing.id = 'EAST';
					break;
				case 'EAST':
					$scope.facing.id = 'NORTH';
					break;
				case 'WEST':
					$scope.facing.id = 'SOUTH';
					break;
				default:
					alert('direction unknown');
			}
		};

		$scope.right = function() {
			switch($scope.facing.id) {
				case 'NORTH':
					$scope.facing.id = 'EAST';
					break;
				case 'SOUTH':
					$scope.facing.id = 'WEST';
					break;
				case 'EAST':
					$scope.facing.id = 'SOUTH';
					break;
				case 'WEST':
					$scope.facing.id = 'NORTH';
					break;
				default:
					alert('direction unknown');
			}
		};

		$scope.report = function() {
			$scope.showPosition = !$scope.showPosition;
		};

		$scope.$watch('xAxis', function(newVal, oldVal) {
			if (newVal == undefined) {
				$scope.xAxis = oldVal;
			}
		});

		$scope.$watch('yAxis', function(newVal, oldVal) {
			if (newVal == undefined) {
				$scope.yAxis = oldVal;
			}
		});

	}]);

	app.directive('gameBoard', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/game-board.html',
			controller: 'gameBoardCtrl'
		};
	});

}) ();
