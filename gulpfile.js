var gulp = require('gulp');
var config = require('./gulp.config')();
var del = require('del');
var $ = require('gulp-load-plugins')({lazy: true, camelize: true});

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('styles', function() {
	return gulp
		.src(config.less)
		.pipe($.plumber())
		.pipe($.less())
		.pipe($.autoprefixer({browsers: ['last 2 versions', '> 5%']}))
		.pipe(gulp.dest(config.css))
		.pipe($.rename({ suffix: '.min' }))
		.pipe($.minifyCss())
		.pipe(gulp.dest(config.css));
});

gulp.task('jsBuild', function() {
	return gulp
		.src(config.srcJs)
		.pipe(gulp.dest(config.prodJs))
		.pipe($.rename({ suffix: '.min' }))
		.pipe($.uglify())
		.pipe(gulp.dest(config.prodJs));
});

gulp.task('copy-index-html', function() {
    gulp.src(config.html)
    .pipe(gulp.dest(config.public));
});

gulp.task('copy-js-lib', function() {
    gulp.src(config.bowerLib)
    .pipe(gulp.dest(config.prodLib));
});

gulp.task('copy-images', function() {
    gulp.src(config.images)
    .pipe(gulp.dest(config.imagesProd));
});

gulp.task('clean-styles', function(done) {
	return clean(config.css + '**/*.css');
});

gulp.task('clean-html', function(done) {
	return clean(config.public + '*.html');
});

gulp.task('clean-js', function(done) {
	return clean(config.js + '**/*.js');
});

gulp.task('clean-images', function(done) {
	return clean(config.imagesProd + '**/*.*');
});

gulp.task('clean-all', ['clean-styles', 'clean-html', 'clean-js', 'clean-images'], function() {
	return;
});

gulp.task('copy-all', ['copy-index-html', 'copy-js-lib', 'copy-images'], function() {

});

gulp.task('watch', ['build'], function() {
	gulp.watch([config.less], ['clean-styles', 'styles']);
	gulp.watch([config.srcJs], ['clean-js', 'copy-js-lib', 'jsBuild']);
	gulp.watch([config.html], ['clean-html', 'copy-index-html']);
	gulp.watch([config.images], ['clean-images', 'copy-images']);
});

gulp.task('build', ['clean-all', 'copy-all', 'styles', 'jsBuild'], function() {
	return;
});

gulp.task('serve', ['watch'], function(){
	serve();
});

///////////////////////////////////////////////////////////////

function clean(path) {
	return del(path);
}

function serve() {
	var nodeOptions = {
		script: config.nodeServer,
		delaytime: 1,
		//watch: [config.server]
	};
	return $.nodemon(nodeOptions);
}
